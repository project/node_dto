<?php

/**

 * 

 * @author Matt Foster

 * This class provides a base class which handles implementation of the Iterator interface

 * This allows us to use the object in native iterable contexts such as foreach.

 * @extends celsius_dto_serialize_base, allows this class to serialize itself into a plethora of formats.

 */

abstract class node_dto_iterable_base extends node_dto_serialize_base implements Iterator{

	protected $hash = array();
	protected $root_tag = 'collection';
	private $index = 0;
	private $flag = false;

	/**

	 *  Implementation of rewind, necessary for Iterator interface

	 *  resets the internal index for the hash array.

	 * @return unknown_type

	 */

	public function rewind(){
		$this->flag = false;
		reset($this->hash);
	}

	/**

	 * Implementation for valid, requisite for Iterator interface

	 * Determines whether the current internal index is at a valid position.

	 * @return unknown_type

	 */

	public function valid(){
		if($this->flag)
			return false;

		if(end(array_values($this->hash)) == $this->current()){
			$this->flag = true;
			return true;
		}
		else{
			return true;
		}
	}

	/**

	 * Implementation for key, requisite for Iterator interface

	 * Returns the key for the current position of the hash array.

	 * @return string

	 */

	public function key(){
		return key($this->hash);
	}

	/**

	 * Implementation of current, requisite for Iterator interface

	 * returns the value for the current position of the hash array.

	 * @return mixed

	 */

	public function current(){
		return current($this->hash);
	}

	/**

	 * Implementation for next, requisite for Iterator interface

	 * Incremenets the internal index of the hash array and returns the value for the new index.

	 * @return mixed

	 */

	public function next(){
		return next($this->hash);
	}

	/**

	 * Implementation of end, requisite for Iterator interface

	 * sets the internal index to the end of the hash array and returns the value for the last index

	 * @return mixed

	 */

	public function end(){
		return end($this->hash);
	}

	public function keys(){
		return array_keys($this->hash);
	}

	public function merge(node_collection $obj){
		foreach($obj as $key => $value){
			if(isset($this->hash[$key]))
				continue;

			$this->hash[$key] = $value;
		}
	}

	/**

	 *  Public method to delegate to protected superclass method of _to_insert_sql which receives

	 *  the parameter of an associative array to serialize into a snippet of SQL syntax to insert into a table.

	 * @return string

	 */

	public function to_insert_sql(){
		return $this->_to_insert_sql($this->hash);
	}

	/**

	 * Public method to delegate to the protected superclass method of _to_update_sql.

	 * Sends the internal property hash to the method and has it parsed into a snippet of

	 * SQL update text to be compiled into a large update query.

	 * @return string

	 */

	public function to_update_sql(){
		return $this->_to_update_sql($this->hash);
	}

	/**

	 * Public method to return the object in a <key>$value</key> XML format.

	 * @return string

	 */

	public function to_xml(){
		$this->parsed = array();
		return $this->_xml($this->hash);
	}

	

	/**

	 * Public method to return the hash object in a JSON String.

	 * @return unknown_type

	 */

	public function to_json(){

		$this->parsed = array();

		return $this->strip_comma($this->_json($this->hash));

	}

	

	/**

	 * Deep casts the entire hash as an object.  In this we can remove all irregularities between stdObject and an associative array.

	 * @return unknown_type

	 */

	public function to_object(){
		return $this->objectify($this->hash);
	}

	/**

	 * Returns the value of the hash, uses magic methods to delegate property accessors to the internal hash reference.

	 * @param $key - string

	 * @return mixed

	 */

	public function __get($key){
		return $this->hash[$key];
	}

	/**

	 * Sets the hash property specified in the key to the inserted value

	 * @param $key

	 * @param $val

	 * @return mixed

	 */

	public function __set($key, $val){
		$this->hash[$key] = $val;
	}

	/**

	 * Magic method implementation to delegate isset accessors to the hash object.

	 * @param $key

	 * @return boolean

	 */

	public function __isset($key){
		return isset($this->hash[$key]);
	}

	/**

	 * delegates unset to the hash object.

	 * @param $key

	 * @return void

	 */

	public function __unset($key){
		unset($this->hash[$key]);
	}	

}

/**

 * An interface to conform collections to a set of methods in which common retrieval cases are already defined, such as first last etc.

 * @author Matt

 *

 */

interface node_dto_collection{

	/**

	 * Returns the first item in the collection

	 * @return mixed

	 */

	function first();

	/**

	 * Returns the last item in the collection

	 * @return mixed

	 */

	function last();

	/**

	 * Returns the item at the specified index

	 * @param $number

	 * @return mixed

	 */

	function item($number);

	/**

	 * Returns the item based on the identified

	 * @param $number

	 * @return mixed

	 */

	function by_id($number);

	/**

	 * Returns a subcollection that has been filtered to only return items that match the type.

	 * @param $string

	 * @return unknown_type

	 */

	function by_type($string);

}



/**

 * Base class to handle basic interface operations for subclasses.

 * @author Matt

 *

 */

abstract class node_dto_collection_base extends node_dto_iterable_base implements node_dto_collection{

	public function first(){
		return $this->item(0);
	}

	public function last(){
		return $this->item(count($this->hash) - 1);
	}

	public function item($number){
		$count = 0;

		foreach($this->hash as $element){
			if($number == $count){
				return $element;
			}
			$count++;
		}
		return false;
	}

	public function by_id($id){
		return $this->hash[$id];
	}

	public function by_type($string){
		throw new Exception(__CLASS__ .' does not implement the method ' . __METHOD__ .' defined in interface. Subclass must overwrite this');
	}

}





/**

 * Subclass of celsius_collection_base to specialize in collections of taxonomy terms.

 * @author Matt

 *

 */

class node_dto_taxonomy_collection extends node_dto_collection_base{

	public function __construct(&$terms = array()){
		$this->hash = $terms;
	}

	/**

	 * Returns all taxonomy terms associated with a particular vocabulary name

	 * @param $type - the vocabulary name

	 */

	public function by_type($type){
		return $this->by_vocab($type);
	}

	public function by_vocab($name){
		$vid = $this->get_vid_from_name($name);
		return $this->by_vid($vid);
	}

	/**

	 * returns all taxonomy terms associated with that vocabulary id

	 * @param $vid

	 * @return unknown_type

	 */

	public function by_vid($vid){
		$collection = array();
		foreach($this->hash as $term){
			if($term -> vid == $vid){
				$collection[$term -> tid] = $term;
			}
		}	

		return new node_dto_taxonomy_collection($collection);
	}

	/**

	 * retrieves the vid for a vocabulary based on name.

	 * @param $name

	 * @return unknown_type

	 */

	public function get_vid_from_name($name){
		$q = "SELECT name, vid FROM {vocabulary} WHERE name='%s' LIMIT 1";

		$res = db_query($q, $name);

		$rec = db_fetch_object($res);		

		return $rec -> vid;
	}

}

