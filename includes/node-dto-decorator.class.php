<?php

class node_dto_decorator extends node_dto_proxy{

	protected $dom;

	protected $cache = false;

	protected $sheet;

	protected $functions;

	public function __construct($node){
		parent :: __construct($node);
		$this-> sheet = new XSLTProcessor();
		$this-> functions = array('theme', 'l', 't', 'url', 'check_url', 'date', 'format_date');
	}

	public function get_php_functions(){
		return $this->functions;
	}

	public function add_php_function($func){
		if(function_exists($func))
		$this->functions[] = $func;
	}

	public function add_php_functions($arr){
		foreach($arr as $func)
		$this->add_php_function($func);
	}

	public function enable_cache(){
		$this->cache = true;
	}

	public function disable_cache(){
		$this->cache = false;
	}

	public function to_dom(){
		if($this->cache && $this->dom){
			return $this->dom;
		}
		else if($this->cache && !$this->dom){
			$this->dom = parent :: to_dom();
			return $this->dom;
		}
		else{
			return parent :: to_dom();
		}
	}
	public function setParameter($name, $value, $namespace = ''){
		$this->sheet->setParameter($namespace, $name, $value);
	}
	
	public function importStyleSheet(DOMDocument $sheet){
		$this->sheet -> registerPHPFunctions($this->get_php_functions());
		$this->sheet -> importStyleSheet($sheet);
	}

	public function transformToDoc(){
		return $this->sheet->transformToDoc($this->to_dom());
	}

	public function transformToXML(){
		return $this->sheet->transformToXML($this->to_dom());
	}

	public function transformToUri($uri){
		return $this->sheet->transformtoUri($this->to_dom(), $uri);
	}
}
