<?php

/**

 * base class for handling property accessor delegation.

 * @author Matt

 *

 */

abstract class node_dto_proxy_base extends node_dto_serialize_base{

	protected $node;	

	public function __get($key){

		return $this->node -> $key;

	}

	public function __set($key, $value){
		$this->node -> $key = $value;
	}

	public function __isset($key){
		return isset($this->node -> $key);
	}

	public function __unset($key){
		unset($this->node -> $key);
	}

	public function __call($method, $args){
		return call_user_func_array(array($this->node, $method), $args);
	}

	public function to_dom(){
		return $this-> _to_dom($this->node);
	}

	public function to_xml(){
		return $this->to_dom() -> saveXML();
	}

}

class node_dto_proxy extends node_dto_proxy_base{

	protected $_taxonomy;

	public function __construct($node){
		$this->node = $node;
		$this->_taxonomy = new node_dto_taxonomy_collection($node->taxonomy);
	}	

	//direct property getters

	public function get_title(){
		return $this->node->title;
	}	

	//taxonomy delegation

	public function get_terms_by_vocab($vocab){
		return $this -> _taxonomy -> by_vocab($vocab);
	}

	public function get_terms_by_vid($vid){
		return $this -> _taxonomy -> by_vid($vid);
	}

}

class user_dto_proxy extends node_dto_proxy_base{
	
	public function __construct($user){
		$this->node = $user;		
	}
	
}

