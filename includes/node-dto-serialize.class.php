<?php



/**

 * Handles object serializations into several formats, such as RPC, XML, JSON 

 * @author Matt Foster

 *

 */

abstract class node_dto_serialize_base{
	protected $parsed = array();
	protected $root_tag = 'node';

	/**

	 * Quick and Dirty display method, returns the object put through the print_r function wrapped

	 * in PRE tags.

	 * @param $obj

	 * @return string

	 */

	public function display($obj){
		return "<pre>".print_r($obj, TRUE)."</pre>";
	}

	/**

	 * Deep casts the structure as an object (stdClass)

	 * @param $struct

	 * @param $obj

	 * @return unknown_type

	 */

	public function objectify($struct, &$obj = false){
		if(!$obj){
			$obj = new stdClass();		
		}
		
		foreach($struct as $key => $value){
			if(is_array($value) || is_object($value)){
				$obj -> $key = new stdClass();
				$this-> objectify($value, $obj -> $key);
			}
			else{
				$obj -> $key = $struct[$key];
			}
		}

		return $obj;
	}



	/**

	 * Receives an associative array and iterates to create a SQL Insert snippet.

	 * Only handles key value pairs, table structure, where clause must be added afterwards.

	 * @param $hash

	 * @return unknown_type

	 */

	protected function _to_insert_sql($hash){

		$keys = "(";

		$vals = "VALUES(";	

		if(is_object($hash)){
			$hash = get_object_vars($hash);
		}
			

		foreach($hash as $key => $val){
			$keys .= $key .",";
			$vals .= "'" . $this->escape_string($val) . "',";
		}		

		return $this->strip_comma($keys). ") ". $this->strip_comma($vals).")";
	}

	/**

	 * Receives an associate array and iterates over it to create a SQL Update snippet.

	 * Only handles key value pairs, table structure, where clause must be added afterwards.

	 * @param $hash

	 * @return unknown_type

	 */

	protected function _to_update_sql($hash){
		$str = "";
		if(is_object($hash)){
			$hash = get_object_vars($hash);
		}
		
		foreach($hash as $key => $val){
			$str .= $key . "='".$this->escape_string($val)."',";
		}
		return $this->strip_comma($str);
	}

	protected function _xml($data){
		return $this->_to_dom($data) -> saveXML();
	}

	protected function _to_dom($data) {
		$doc = new DOMDocument('1.0', 'utf-8');
		$root = $this->create_root($doc);
		$doc->appendChild($root);
		$this->xml_recurse($doc, $root, $data);
		return $doc;

	}

	protected function create_root($doc){
		return $doc->createElement($this->root_tag);
	}
	
	public function set_root_tag($tag){
		$this->root_tag = $tag;
	}
	
	/**
	 * The main processing function of serializing a struct/Object into an XMLDocument
	 * Largely taken from the services module with some modifications to make tag names XML Safe and another feature
	 * to avoid endless recursion caused by back references.
	 * @param $doc -- DOMDocument, original point of reference.
	 * @param $parent -- Parent node that the new nodes in this execution will be appended to.
	 * @param $data  -- the data object.
	 */	 
	protected function xml_recurse(&$doc, &$parent, $data) {

		if (is_object($data)) {
			$data = get_object_vars($data);
		}		

		if($this->is_parsed($data)){
			$element = $doc -> createElement('recursion');
			$parent -> appendChild($element);
		}

		else if (is_array($data)) {
			$assoc = FALSE || empty($data);

			$this->parsed[] = $data;

			foreach ($data as $key => $value) {
				if (is_numeric($key)) {
					$key = 'item';
				}

				else {
					$assoc = TRUE;
					$key = preg_replace('/[^a-zA-Z0-9_]/', '_', $key);
					$key = preg_replace('/^([0-9]+)/', '_$1', $key);
				}

				$element = $doc->createElement($key);				

				$parent->appendChild($element);

				$this->xml_recurse($doc, $element, $value);
			}

			array_pop($this->parsed);
			if (!$assoc) {
				$parent->setAttribute('is_array', 'true');
			}
		}

		else if ($data!==NULL) {
			$parent->appendChild($doc->createTextNode($data));
		}

	}

	/**

	 * Receives an object and parses it into a JSON formatted string

	 * { $key : '$value' }

	 * @param $obj

	 * @param $p_key

	 * @return unknown_type

	 */

	protected function _json($obj){
		return json_encode($obj);
	}	

	/**

	 * Receives an array and parses the values into a CSV structured string

	 * @param $arr

	 * @return unknown_type

	 */

	protected function to_csv($arr){
		foreach($arr as $val) {
			$str .= $this->wash_value($val). ",";			
		}		
		return substr($str, 0, strlen($str)-1);
	}

	

	/**

	 * Method used to avoid endless recursion when encountering a back reference during serialization.

	 * @param $obj

	 * @return unknown_type

	 */

	protected function is_parsed($obj){
		if(!(is_array($obj) || is_object($obj))){
			return false;
		}

		foreach($this->parsed as $val){
			if($val === $obj)
				return true;
		}
		return false;
	}

	/* * Removes the last comma in a string.

	 */

	protected function strip_comma($str){
		return ((strrpos($str, ",") === false) ? $str : substr($str, 0, strrpos($str, ",")));
	}	

}
